import React from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

const Post = ({ data }) => {
  console.log(data);
  return null;
};

export default graphql(gql`
  query TodoAppQuery {
    query
    getUSers {
      users(take: 10) {
        id
        name
        primaryRCCChapter {
          id
          name
          location {
            id
            lat
            lng
          }
        }
      }
    }
  }
`)(Post);
