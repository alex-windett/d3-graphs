import React, { useEffect, useState, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

const createGraph = ({ data, ref }) => {};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    createGraph((data, ref));
  }, [ref]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
