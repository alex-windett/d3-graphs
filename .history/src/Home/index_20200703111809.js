import React, { useEffect, useState, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";

const createGraph = ({ data, ref }) => {
  // var svg = d3.select(ref.current).append(svg);
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    console.log(ref);
  }, [ref]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
