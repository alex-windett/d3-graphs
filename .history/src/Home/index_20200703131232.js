import React, { useEffect, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";
import { countBy } from "lodash";

const width = 300;
const height = 300;
const baseCircleSize = 1;
const collisionRadius = 20;
const circleFill = { color: "rgb(246 117 153 / 50%)", opacity: 0.1 };
const circleStroke = {
  color: "#b3a2c8",
  width: 1,
};

const createCircles = ({ svg, data }) => {
  let dataset = data.users.filter((user) => !!user.primaryRCCChapter);
  dataset = dataset.map((user) => user.primaryRCCChapter.name);

  dataset = countBy(dataset);

  dataset = Object.keys(dataset).reduce((acc, curr) => [...acc, { [curr]: dataset[curr] }], []);

  const dragstarted = (d) => {
    if (!d3.event.active) simulation.alphaTarget(0.03).restart();
    d.fx = d.x;
    d.fy = d.y;
  };
  const dragged = (d) => {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  };
  const dragended = (d) => {
    if (!d3.event.active) simulation.alphaTarget(0.03);
    d.fx = null;
    d.fy = null;
  };

  const node = svg.append("g").selectAll("circle").data(dataset).enter();

  const circleNode = node
    .append("g")
    .append("circle")
    .attr("class", "circle-node")
    .attr("r", function (d) {
      const [name, count] = Object.entries(d)[0];
      return baseCircleSize * count;
    })
    .attr("cx", width / 2)
    .attr("cy", height / 2)
    .style("fill", circleFill.color)
    .style("fill-opacity", circleFill.opactiy)
    .attr("stroke", circleStroke.color)
    .style("stroke-width", circleStroke.width)
    .call(d3.drag().on("start", dragstarted).on("drag", dragged).on("end", dragended));

  d3.select(circleNode).each((d) => {
    console.log(d);
    // Clubhouse Object : { [name]: [count] }
    // const [name] = Object.keys(d);
    // console.log(name);
    // const slug = name.replace(" ", "-").toLowerCase();
    // d3.select(this).enter().attr("class", `circle-node circle-node-${slug}`).add("circle-node-");
  });

  const simulation = d3
    .forceSimulation()
    .force(
      "center",
      d3
        .forceCenter()
        .x(width / 2)
        .y(height / 2)
    )
    .force("charge", d3.forceManyBody().strength(1)) // Nodes are attracted one each other of value is > 0
    .force("collide", d3.forceCollide().strength(0.1).radius(collisionRadius).iterations(1)); // Force that avoids circle overlapping

  simulation.nodes(dataset).on("tick", (d) => {
    circleNode.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
  });
};

const createGraph = ({ data, ref }) => {
  var svg = d3
    .select(ref.current)
    .append("svg")
    .attr("preserveAspectRatio", "xMinYMin")
    .attr("viewBox", `0 0 ${width} ${height}`)
    .classed("svg-content-responsive", true);

  createCircles({ svg, data });
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current && data.users) {
      createGraph({ ref, data });
    }
  }, [data]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
