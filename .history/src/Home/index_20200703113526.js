import React, { useEffect, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";

const width = 500;
const height = 500;

const createCircles = ({ svg, data }) => {
  const g = svg.append("g");

  // console.log(data);

  let dataset = data.users.filter((user) => !!user.primaryRCCChapter);
  dataset = dataset.map((user) => user.primaryRCCChapter.name);

  dataset = dataset.reduce((acc, curr) => {
    console.log(acc);
    return {
      ...acc,
      [curr]: acc[curr] ? acc[curr] + 1 : 0,
    };
  }, {});

  console.log(dataset);

  // g.selectAll("circle")
  //   .data(data)
  //   .join("circle")
  //   .attr("cx", (d) => d[0])
  //   .attr("cy", (d) => d[1])
  //   .attr("r", 3);
};

const createGraph = ({ data, ref }) => {
  var svg = d3
    .select(ref.current)
    .append("svg")
    .attr("preserveAspectRatio", "xMinYMin")
    .attr("viewBox", `0 0 ${width} ${height}`)
    .classed("svg-content-responsive", true);

  createCircles({ svg, data });
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current) {
      createGraph({ ref, data });
    }
  }, [data]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
