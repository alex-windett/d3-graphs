import React, { useEffect, useState, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

const createGraph = () => {};

const Post = ({ data }) => {
  const graphRef = useRef(null);

  useEffect(() => {
    createGraph(data);
  }, [graphRef]);

  if (data.loading) return <p>...loading</p>;

  console.log(data.users);
  return <div ref={graphRef}></div>;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
