import React, { useEffect, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";
import { countBy } from "lodash";

const width = 500;
const height = 500;

const createCircles = ({ svg, data }) => {
  let dataset = data.users.filter((user) => !!user.primaryRCCChapter);
  dataset = dataset.map((user) => user.primaryRCCChapter.name);

  dataset = countBy(dataset);

  dataset = Object.keys(dataset).reduce((acc, curr) => [...acc, { [curr]: dataset[curr] }], []);

  // Initialize the circle: all located at the center of the svg area
  var node = svg
    .append("g")
    .selectAll("circle")
    .data(data)
    .enter()
    .append("circle")
    .attr("r", 25)
    .attr("cx", width / 2)
    .attr("cy", height / 2)
    .style("fill", "#19d3a2")
    .style("fill-opacity", 0.3)
    .attr("stroke", "#b3a2c8")
    .style("stroke-width", 4)
    .call(
      d3
        .drag() // call specific function when circle is dragged
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended)
    );

  // Features of the forces applied to the nodes:
  var simulation = d3
    .forceSimulation()
    .force(
      "center",
      d3
        .forceCenter()
        .x(width / 2)
        .y(height / 2)
    ) // Attraction to the center of the svg area
    .force("charge", d3.forceManyBody().strength(1)) // Nodes are attracted one each other of value is > 0
    .force("collide", d3.forceCollide().strength(0.1).radius(30).iterations(1)); // Force that avoids circle overlapping

  // Apply these forces to the nodes and update their positions.
  // Once the force algorithm is happy with positions ('alpha' value is low enough), simulations will stop.
  simulation.nodes(data).on("tick", function (d) {
    node
      .attr("cx", function (d) {
        return d.x;
      })
      .attr("cy", function (d) {
        return d.y;
      });
  });

  // What happens when a circle is dragged?
  function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.03).restart();
    d.fx = d.x;
    d.fy = d.y;
  }
  function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
  }
  function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0.03);
    d.fx = null;
    d.fy = null;
  }
};

const createGraph = ({ data, ref }) => {
  var svg = d3
    .select(ref.current)
    .append("svg")
    .attr("preserveAspectRatio", "xMinYMin")
    .attr("viewBox", `0 0 ${width} ${height}`)
    .classed("svg-content-responsive", true);

  createCircles({ svg, data });
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current) {
      createGraph({ ref, data });
    }
  }, [data]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
