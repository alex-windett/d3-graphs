import React, { useEffect, useState } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

const Post = ({ data }) => {
  useEffect(() => {
    createGraph(data);
  }, []);

  if (data.loading) return <p>...loading</p>;

  console.log(data.users);
  return null;
};

export default graphql(gql`
  query getUSers {
    users(take: 50) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`)(Post);
