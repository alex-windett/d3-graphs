import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Table } from "reactstrap";

export const GET_POSTS = gql`
  query getUSers {
    users(take: 10) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`;

export default () => (
  <Query query={GET_POSTS}>
    {({ loading, data }) =>
      !loading && (
        <>
          {console.log(data)}
          <p>hello</p>
        </>
      )
    }
  </Query>
);
