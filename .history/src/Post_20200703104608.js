import React from "react";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Table } from "reactstrap";

export const GET_POSTS = gql`
  query getUSers {
    users(take: 10) {
      id
      name
      primaryRCCChapter {
        id
        name
        location {
          id
          lat
          lng
        }
      }
    }
  }
`;

export default () => (
  <Query query={GET_POSTS}>
    {({ loading, data }) =>
      !loading && (
        <Table>
          <thead>
            <tr>
              <th>Author</th>
              <th>Body</th>
            </tr>
          </thead>
          <tbody>
            {data.posts.map((post) => (
              <tr key={post.id}>
                <td>{post.author}</td>
                <td>{post.body}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      )
    }
  </Query>
);
