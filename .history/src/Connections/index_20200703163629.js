import React, { useEffect, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";
import { uniq } from "lodash";
import "./App.css";

const width = 2000;
const height = 2000;
const baseCircleSize = 1;
const collisionRadius = 150;
const black = "#000";
const circleFill = { color: "rgb(246 117 153 / 50%)", opacity: 0.1 };
const circleStroke = {
  color: "#b3a2c8",
  width: 1,
};

const createConnections = ({ svg, data }) => {
  // const data = {nodes: [{...}] links: [{ ... }]};

  const { user } = data;

  const flattenUsers = user.following.flatMap((x) => x.following.flatMap((y) => y));

  let uniqueUsers = uniq(flattenUsers);
   uniqueUsers = uniqueUsers.map( user => user.name);

  const nodes = uniqueUsers.map((user, i) => ({ id: user, name: user }));
  debugger;


  const links = () => uniqueUsers.map((user) => {
    const commonConnections
  })
  const links = uniqueUsers.map((user, i) => ({ source: user, target }));
};

const createGraph = ({ data, ref }) => {
  var svg = d3
    .select(ref.current)
    .append("svg")
    .attr("preserveAspectRatio", "xMinYMin")
    .attr("viewBox", `0 0 ${width} ${height}`)
    .classed("svg-content-responsive", true);

  createConnections({ data, svg });
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current && data.user) {
      createGraph({ ref, data });
    }
  }, [data]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query someUser {
    user(where: { email: "tom.hutchinson@rapha.cc" }) {
      name
      activities {
        id
      }
      following {
        id
        name
        following {
          name
          id
        }
      }
    }
  }
`)(Post);
