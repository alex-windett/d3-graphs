import React, { useEffect, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";
import _, { uniq } from "lodash";
import "./App.css";

const baseCircleSize = 1;
const collisionRadius = 150;
const black = "#000";
const circleFill = { color: "rgb(246 117 153 / 50%)", opacity: 0.1 };
const circleStroke = {
  color: "#b3a2c8",
  width: 1,
};

var margin = { top: 10, right: 30, bottom: 30, left: 40 },
  width = 15000 - margin.left - margin.right,
  height = 20000 - margin.top - margin.bottom;

const createConnections = ({ svg, data }) => {
  // const data = {nodes: [{...}] links: [{ ... }]};

  const { user } = data;

  const flattenUsers = user.following.flatMap((x) => x.following.flatMap((y) => y));

  const uniqueUsers = uniq(flattenUsers);

  const nodes = uniqueUsers.map((user) => ({ id: user.id, name: user.name }));

  let links = user.following.flatMap((f) =>
    f.following.map((x) => ({
      source: x.id,
      target: f.id,
    }))
  );

  const uniqueIDS = uniqueUsers.map((user) => user.id);

  links = links.filter((link) => uniqueIDS.includes(link.target));

  // Initialize the links
  var link = svg
    .selectAll("line")
    .data(links)
    .enter()
    .append("line")
    .style("stroke", "#aaa")
    .style("stroke-width", 2);

  // Initialize the nodes
  var circle = svg
    .selectAll("circle")
    .data(nodes)
    .enter()
    .append("circle")
    .attr("r", 30)
    .style("fill", "#69b3a2")
    .on("mouseover", (d) => {
      console.log(d);
      svg.append("g").text(d.name).attr("x", d.x).attr("y", d.y);
    });

  // Let's list the force we wanna apply on the network
  var simulation = d3
    .forceSimulation(nodes) // Force algorithm is applied to data.nodes
    .force(
      "link",
      d3
        .forceLink() // This force provides links between nodes
        .id((d) => d.id) // This provide  the id of a node
        .links(links) // and this the list of links
    )
    .force("charge", d3.forceManyBody().strength(-3000)) // This adds repulsion between nodes. Play with the -400 for the repulsion strength
    .force("center", d3.forceCenter(width / 2, height / 2)) // This force attracts nodes to the center of the svg area
    .on("end", ticked);

  // This function is run at each iteration of the force algorithm, updating the nodes position.
  function ticked() {
    link
      .attr("x1", (d) => d.source.x)
      .attr("y1", (d) => d.source.y)
      .attr("x2", (d) => d.target.x)
      .attr("y2", (d) => d.target.y);

    circle.attr("cx", (d) => d.x + 6).attr("cy", (d) => d.y - 6);
  }
};

const createGraph = ({ data, ref }) => {
  var svg = d3
    .select(ref.current)
    .append("svg")
    .attr("preserveAspectRatio", "xMinYMin")
    .attr(
      "viewBox",
      `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`
    )
    .classed("svg-content-responsive", true)
    .append("g");

  createConnections({ data, svg });
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current && data.user) {
      createGraph({ ref, data });
    }
  }, [data]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query someUser {
    user(where: { email: "tom.hutchinson@rapha.cc" }) {
      name
      activities {
        id
      }
      following {
        id
        name
        following {
          name
          id
        }
      }
    }
  }
`)(Post);
