import React, { useEffect, useRef } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";
import * as d3 from "d3";
import _, { uniq } from "lodash";
import "./App.css";

const width = 2000;
const height = 2000;
const baseCircleSize = 1;
const collisionRadius = 150;
const black = "#000";
const circleFill = { color: "rgb(246 117 153 / 50%)", opacity: 0.1 };
const circleStroke = {
  color: "#b3a2c8",
  width: 1,
};

const createConnections = ({ svg, data }) => {
  // const data = {nodes: [{...}] links: [{ ... }]};

  const { user } = data;

  const flattenUsers = user.following.flatMap((x) => x.following.flatMap((y) => y));

  const uniqueUsers = uniq(flattenUsers);
  const uniqueUsersNames = uniqueUsers.map((user) => user.name);

  const nodes = uniqueUsersNames.map((user, i) => ({ id: user, name: user }));

  let links = user.following.flatMap((f) =>
    f.following.map((x) => ({
      source: x.name,
      target: f.name,
    }))
  );

  var link = svg.selectAll("line").data(data.links).enter().append("line").style("stroke", "#aaa");

  // Initialize the nodes
  var node = svg
    .selectAll("circle")
    .data(data.nodes)
    .enter()
    .append("circle")
    .attr("r", 20)
    .style("fill", "#69b3a2");

  // Let's list the force we wanna apply on the network
  var simulation = d3
    .forceSimulation(data.nodes) // Force algorithm is applied to data.nodes
    .force(
      "link",
      d3
        .forceLink() // This force provides links between nodes
        .id(function (d) {
          return d.id;
        }) // This provide  the id of a node
        .links(data.links) // and this the list of links
    )
    .force("charge", d3.forceManyBody().strength(-400)) // This adds repulsion between nodes. Play with the -400 for the repulsion strength
    .force("center", d3.forceCenter(width / 2, height / 2)) // This force attracts nodes to the center of the svg area
    .on("end", ticked);

  // This function is run at each iteration of the force algorithm, updating the nodes position.
  function ticked() {
    link
      .attr("x1", function (d) {
        return d.source.x;
      })
      .attr("y1", function (d) {
        return d.source.y;
      })
      .attr("x2", function (d) {
        return d.target.x;
      })
      .attr("y2", function (d) {
        return d.target.y;
      });

    node
      .attr("cx", function (d) {
        return d.x + 6;
      })
      .attr("cy", function (d) {
        return d.y - 6;
      });
  }
};

const createGraph = ({ data, ref }) => {
  var svg = d3
    .select(ref.current)
    .append("svg")
    .attr("preserveAspectRatio", "xMinYMin")
    .attr("viewBox", `0 0 ${width} ${height}`)
    .classed("svg-content-responsive", true);

  createConnections({ data, svg });
};

const Post = ({ data }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (ref && ref.current && data.user) {
      createGraph({ ref, data });
    }
  }, [data]);

  if (data.loading) return <p>...loading</p>;

  return <div ref={ref}></div>;
};

export default graphql(gql`
  query someUser {
    user(where: { email: "tom.hutchinson@rapha.cc" }) {
      name
      activities {
        id
      }
      following {
        id
        name
        following {
          name
          id
        }
      }
    }
  }
`)(Post);
