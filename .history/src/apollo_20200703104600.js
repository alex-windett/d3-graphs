import ApolloClient from "apollo-boost";

export default new ApolloClient({
  uri: "https://rides-test-api.raphadev.cc",
});
